import {BrowserRouter, Route, Routes} from 'react-router-dom'
import {Home} from "./pages/Home";
import {Collection_points} from "./pages/Collection_points";
import {Navbar} from "./components/Navbar";
import {Footer} from "./components/Footer";

function App() {
  return (
      <BrowserRouter>
        <div className="main">
      <Navbar/>
        <div className="content">
          <Routes>
            <Route path={'/'} exact element={<Home/>}/>
            <Route path={'/collection_points'} element={<Collection_points/>}/>
          </Routes>
        </div>
          <Footer/>
        </div>
      </BrowserRouter>
  );
}

export default App;
