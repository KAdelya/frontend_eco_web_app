import {Fragment} from "react";
import {Swipe} from "../components/Swipers_and_Slides";
import {SmallSlides} from "../components/Swipers_and_Slides";
import map from "../components/images/map.svg";
import ecomarket from '../components/images/ecomarket.svg'

export const Home = () => {
    return (
        <Fragment>
            <Swipe/>
            <div className='small_slides'>
                <div className="first_slide">
                    <SmallSlides quote="Пункты сбора"
                                 main_idea="Посмотри, где в твоем городе можно сдать вторсырье на переработку"
                                 img={map}
                    />
                </div>
                <div className="second_slide">
                    <SmallSlides quote="ЭкоМаркет"
                                 main_idea="Используй заработанные экокоины для покупки товаров из переработанных материалов "
                                 img={ecomarket}/>
                </div>
            </div>
        </Fragment>
    )
}
