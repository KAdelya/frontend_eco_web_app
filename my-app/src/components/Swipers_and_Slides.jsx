import './Swipers_and_Slides.scss';
import make_the_world_cleaner from "./images/make_the_world_cleaner.svg";
import button from "./images/button.svg";
import and_you_know from "./images/and_you_know.svg";
import mask from "./images/mask.svg"
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation } from 'swiper';
import 'swiper/scss';
import 'swiper/scss/navigation';

export const BigSlides = ({quote, main_idea, button, color, classN, alt}) => {
    return (
        <div className='big_slides_wrapper' style={{'background': color}}>
            <div style={{'padding': '60px 0px 0px 68px'}}>
                <h1 style={{'font-weight':'bold'}}>{quote}</h1>
                <div>{main_idea}</div>
                <button className='button_style'>{button}</button>
            </div>
            <img src={classN} className="poster" alt={alt}/>
        </div>
    )
}

export const SmallSlides = ({quote, main_idea, color, img, alt}) => {
    return (
        <div className='small_slides_wrapper' style={{'background': color}}>
            <div>
                <h1>{quote}</h1>
                <div>{main_idea}</div>
                <button style={{'border': 'none', 'cursor': 'pointer', 'background':'none', 'padding-top': '10%'}}>
                    <img src={button}/></button>
            </div>
            <img src={img} alt={alt} style={{'margin-left': '0px', 'border-radius': '16px'}}/>
        </div>
    )
}

export const Swipe = () => {
    return (
        <div className='swiper-wrapper'>
            <Swiper
                modules={[Navigation]}
                slidesPerView={1}
                navigation
            >
                <SwiperSlide>
                    <BigSlides quote="Сделаем мир чище"
                      main_idea="Сдай макулатуру или старую одежду и получи скидку на покупку товаров из переработанных материалов"
                      button="Условия сервиса" color='#B3EDC8'
                      classN={make_the_world_cleaner} alt="Утилизация отходов"/>
                </SwiperSlide>
                <SwiperSlide>
                    <BigSlides quote="А вы знали..."
                      main_idea="что среднее время разложения пластмассовых изделий колеблется от 400 до 700 лет,
                       а полиэтиленовых пакетов — от 100 до 200 лет?"
                      button="Узнать больше" color='#FFE48F' classN={and_you_know} alt="Пластмассовое изделие"/>
                </SwiperSlide>
                <SwiperSlide>
                    <BigSlides quote="Что с масками?"
                      main_idea="Медицинские маски не обязательно должны становиться отходами.
                      Их тоже можно сдать на переработку."
                      button="Пункты сбора масок" color='#BFF0DE' classN={mask} alt="Маска"/>
                </SwiperSlide>
            </Swiper>
        </div>
    )
}
