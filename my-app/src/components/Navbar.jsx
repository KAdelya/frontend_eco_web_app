import {NavLink} from "react-router-dom";
import styles from './Navbar.module.scss';
import logo from "./images/logo.svg";
import gps from "./images/gps.svg";
import e from "./images/у.svg";
import photo_profile from "./images/photo_profile.svg"

export const Navbar = () => (
    <header>
        <nav>
            <img src={logo} className={styles.logo} alt="Лого"/>
            <NavLink className={styles.link} to='/' exact>Главная</NavLink>
            <NavLink className={styles.link} to='/collection_points'>Пункты сбора</NavLink>
            <NavLink className={styles.link} to='/eco-market'>ЭкоМаркет</NavLink>
            <NavLink className={styles.link} to='/about-service'>О сервисе</NavLink>
            <img src={gps} className={styles.gps} alt="Местоположение"/>
            <a href="#" className={styles.town}>Казань</a>
            <img src={e} className={styles.e} alt="Знак"/>
            <a href="#" className={styles.after_e}>1000</a>
            <img src={photo_profile} className={styles.photo_profile} alt="Фото профиля"/>
            <a href="#" className={styles.name_profile}>Алексей</a>
        </nav>
    </header>
)
