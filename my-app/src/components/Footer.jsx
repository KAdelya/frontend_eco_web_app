import styles from './Footer.module.scss';
import mail from "./images/mail.svg";
import phone from "./images/phone.svg";

export const Footer = () => (
    <footer>
        <div className={styles.nav_bottom}>
            <img src={mail} className={styles.mail_image}/>
            <a href="#" className={styles.mail_text}>info@ecorus.ru</a>
            <img src={phone} className={styles.phone_image}/>
            <a href="tel:+7 (800) 880-88-88" className={styles.phone_text}>+7 (800) 880-88-88</a>
        </div>
    </footer>
)
